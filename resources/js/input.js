(function (window, document, Vue) {
  document.addEventListener('DOMContentLoaded', function () {
    const startEl = document.querySelector('[name="start"]')

    const Lesson = {
      name: 'Lesson',
      template: '#lesson-tpl',

      data () {
        return {
          top: 0,
          left: 0,
          trTop: 0,
          trLeft: 0,
        }
      },

      props: {
        totalHoursInDay: { type: Number, default: () => 12 },
        totalHoursOffset: { type: Number, default: () => 8 },
        lesson: { type: Object, default: () => ({}) },
        mapperWidth: { type: Number, default: () => 0 },
      },

      computed: {
        totalFiveMinutes () {
          return this.totalHoursInDay * 12
        },

        date () {
          return new Date(this.lesson.start)
        },

        styleTop () {
          const day = this.date.getDay() || 7

          return `${day * 30}px`
        },

        styleWidth () {
          return `${this.percentOffset(this.$parent.course.duration)}%`
        },

        styleLeft () {
          return (this.date.getHours() - this.totalHoursOffset)
            * (100 / this.totalHoursInDay)
            + this.percentOffset(this.date.getMinutes()) + '%'
        },

        styleTransform () {
          return `translate(${this.trLeft}px, ${this.trTop}px)`
        },

        style () {
          return {
            top: this.styleTop,
            width: this.styleWidth,
            left: this.styleLeft,
            transform: this.styleTransform,
          }
        },

        time () {
          const match = this.lesson.start
            .match(/\d{4}-\d{2}-\d{2} (\d{2}:\d{2}):\d{2}/)

          return match[1]
        },
      },

      methods: {
        percentOffset (mins) {
          return (mins / 5) * (100 / this.totalFiveMinutes)
        },

        nn: (val) => val > 9 ? String(val) : String('0' + val),

        removeLesson (e) {
          this.$emit('lesson:remove', this.lesson)
        },

        getDate () {
          const date = new Date(Date.parse(this.date))
          const fiveMins = this.trLeft / this.$parent.$el.offsetWidth * this.totalFiveMinutes

          date.setDate(date.getDate() - parseInt(this.trTop / 30))
          date.setHours(date.getHours() + parseInt(fiveMins / 12 + this.totalHoursOffset))
          date.setMinutes(date.getMinutes() + fiveMins % 12)

          return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} `
            + `${this.nn(date.getHours())}:${this.nn(date.getMinutes())}:00`
        },

      },
    }

    const Mapper = {
      name: 'Mapper',
      template: '#mapper-tpl',
      components: { Lesson },

      props: {
        course: { type: Object, default: () => ({}) },
        totalHoursInDay: { type: Number, default: () => 12 },
        totalHoursOffset: { type: Number, default: () => 8 },
      },

      data () {
        return {
          lessons: [],
          date: startEl.value,
        }
      },

      computed: {
        totalFiveMinutes () {
          return this.totalHoursInDay * 12
        },

        width () {
          return this.$el && this.$el.offsetWidth || 0
        },

        daysOfWeek () {
          return [
            `${this.dates && this.dates.mo.toISOString().slice(5, 10) || ''} Mo`,
            `${this.dates && this.dates.tu.toISOString().slice(5, 10) || ''} Tu`,
            `${this.dates && this.dates.we.toISOString().slice(5, 10) || ''} We`,
            `${this.dates && this.dates.th.toISOString().slice(5, 10) || ''} Th`,
            `${this.dates && this.dates.fr.toISOString().slice(5, 10) || ''} Fr`,
            `${this.dates && this.dates.sa.toISOString().slice(5, 10) || ''} Sa`,
            `${this.dates && this.dates.su.toISOString().slice(5, 10) || ''} Su`,
          ]
        },

        startOfWeek () {
          const date = new Date(this.date)
          const day = date.getDay()

          date.setDate(date.getDate() - day + (day === 0 ? -6 : 1))

          return new Date(date)
        },

        dates () {
          return {
            mo: this.startOfWeek,
            tu: new Date((new Date(this.startOfWeek)).setDate(this.startOfWeek.getDate() + 1)),
            we: new Date((new Date(this.startOfWeek)).setDate(this.startOfWeek.getDate() + 2)),
            th: new Date((new Date(this.startOfWeek)).setDate(this.startOfWeek.getDate() + 3)),
            fr: new Date((new Date(this.startOfWeek)).setDate(this.startOfWeek.getDate() + 4)),
            sa: new Date((new Date(this.startOfWeek)).setDate(this.startOfWeek.getDate() + 5)),
            su: new Date((new Date(this.startOfWeek)).setDate(this.startOfWeek.getDate() + 6)),
          }
        },
      },

      methods: {
        nn: (val) => val > 9 ? String(val) : String('0' + val),

        onCellClick (e) {
          this.$emit('lesson:create', {
            course_id: this.course.id,
            start: e.target.dataset.date,
          })
        },

        cellDate (day, min) {
          const date = new Date(this.startOfWeek)

          date.setDate(date.getDate() + day)
          date.setHours(parseInt(min / 60 + this.totalHoursOffset))
          date.setMinutes(min % 60)

          return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
            + ` ${this.nn(date.getHours())}:${this.nn(date.getMinutes())}:00`
        },

        cellTitle (mins) {
          return `${parseInt(mins / this.totalHoursInDay) + this.totalHoursOffset}:${this.nn(mins % this.totalHoursInDay * 5)}`
        },

        removeLesson (payload) {
          const vm = this
          const xhr = new XMLHttpRequest()
          const params = this.serialize({ id: payload.id })

          xhr.addEventListener('load', function () {
            vm.lessons.splice(
              vm.lessons.findIndex((lesson) => lesson.id === payload.id),
              1
            )
          });

          xhr.open('POST', `/admin/lessons/week/remove?${params}`)
          xhr.send()
        },

        createLesson (lesson) {
          const vm = this
          const xhr = new XMLHttpRequest()
          const params = this.serialize({
            course: lesson.course_id,
            start: lesson.start,
          })

          xhr.addEventListener('load', function () {
            vm.lessons.push(JSON.parse(this.response))
          });

          xhr.open('POST', `/admin/lessons/week/add?${params}`)
          xhr.send()
        },

        fetchLessons () {
          const vm = this
          const xhr = new XMLHttpRequest()
          const params = this.serialize({
            course: this.course.id,
            date: `${this.date} 00:00:00`,
          })

          xhr.addEventListener('load', function () {
            vm.lessons = JSON.parse(this.response)
          });

          xhr.open('POST', `/admin/lessons/week/load?${params}`)
          xhr.send()
        },

        goPrev () {
          const date = new Date(startEl.value)
          date.setDate(date.getDate() - 7)

          startEl._flatpickr.setDate(date)
          startEl.dispatchEvent(new Event('change'))
        },

        goNext () {
          const date = new Date(startEl.value)
          date.setDate(date.getDate() + 7)

          startEl._flatpickr.setDate(date)
          startEl.dispatchEvent(new Event('change'))
        },

        cloneWeek () {
          const vm = this
          const xhr = new XMLHttpRequest()

          xhr.addEventListener('load', function () {
            vm.goNext()
            vm.lessons = JSON.parse(this.response)
          })

          xhr.open('POST', '/admin/lessons/week/clone')
          xhr.setRequestHeader('Content-Type', 'application/json')
          xhr.send(JSON.stringify({ lessons: this.lessons }))
        },

        serialize (obj, prefix) {
          let str = [], p

          for(p in obj) {
            if (obj.hasOwnProperty(p)) {
              let k = prefix ? `${prefix}[${p}]` : p
              let v = obj[p]

              str.push((v !== null && typeof v === 'object')
                ? this.serialize(v, k)
                : encodeURIComponent(k) + '=' + encodeURIComponent(v))
            }
          }

          return str.join('&')
        }
      },

      created () {
        this.$on('lesson:create', this.createLesson)
        this.fetchLessons()

        startEl.addEventListener('change', (e) => {
          this.date = e.target.value
          this.fetchLessons()
        })
      },
    }

    new Vue({
      el: '.mapper-wrapper',
      components: { Mapper },
    })
  })
})(window, document, Vue)
