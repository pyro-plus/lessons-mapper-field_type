<?php namespace Defr\LessonsMapperFieldType\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\ResourceController;
use Carbon\Carbon;
use Defr\LessonsModule\Lesson\Contract\LessonRepositoryInterface;

/**
 * Class LessonsController command
 *
 * @category Streams_Platform_Addon
 * @package  LayoutFieldType
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class LessonsController extends ResourceController
{

    /**
     * Loads a week of lessons
     *
     * @param   LessonRepositoryInterface  $lessons  The lessons
     * @return  Response
     */
    public function week(LessonRepositoryInterface $lessons)
    {
        // if (!$this->request->ajax()) {
        //     return $this->redirect->back();
        // }

        return $this->response->json($lessons->findWeekByDateAndCourse(
            Carbon::parse($this->request->get('date')),
            $this->request->get('course')
        ));
    }

    /**
     * Adds a lesson to a store
     *
     * @param   LessonRepositoryInterface  $lessons  The lessons
     * @return  Response
     */
    public function add(LessonRepositoryInterface $lessons)
    {
        if (!$lesson = $lessons->create([
            'course_id' => $this->request->get('course'),
            'start'     => $this->request->get('start'),
        ])) {
            return $this->response->json(['error' => true]);
        }

        return $this->response->json($lesson);
    }

    /**
     * Removes lesson from store
     *
     * @param   LessonRepositoryInterface  $lessons  The lessons
     * @return  Response
     */
    public function remove(LessonRepositoryInterface $lessons)
    {
        if (!$lesson = $lessons->find($this->request->get('id'))) {
            return $this->response->json(['error' => true]);
        }

        return $this->response->json([
            'success' => $lessons->delete($lesson),
        ]);
    }

    /**
     * Clones the week of lessons to the next week
     *
     * @param   LessonRepositoryInterface  $lessons  The lessons
     * @return  Response
     */
    public function clone(LessonRepositoryInterface $lessons)
    {
        if (!$oldWeek = $this->request->get('lessons')) {
            return $this->response->json(['error' => true]);
        }

        $newWeek = [];

        foreach ($oldWeek as $lesson) {
            $start = Carbon::parse(array_get($lesson, 'start'))->addWeeks(1);

            $newWeek[] = $lessons->create(
                [
                    'start'     => $start,
                    'course_id' => array_get($lesson, 'course_id'),
                ]
            );
        }

        return $this->response->json($newWeek);
    }

}
