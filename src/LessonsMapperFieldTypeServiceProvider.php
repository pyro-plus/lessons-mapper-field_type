<?php namespace Defr\LessonsMapperFieldType;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

/**
 * Class LessonsMapperFieldTypeServiceProvider command
 *
 * @category Streams_Platform_Addon
 * @package  LayoutFieldType
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class LessonsMapperFieldTypeServiceProvider extends AddonServiceProvider
{

    /**
     * Addon routes
     *
     * @var  array
     */
    protected $routes = [
        'admin/lessons/week/load'   => 'Defr\LessonsMapperFieldType\Http\Controller\LessonsController@week',
        'admin/lessons/week/add'    => 'Defr\LessonsMapperFieldType\Http\Controller\LessonsController@add',
        'admin/lessons/week/remove' => 'Defr\LessonsMapperFieldType\Http\Controller\LessonsController@remove',
        'admin/lessons/week/clone'  => 'Defr\LessonsMapperFieldType\Http\Controller\LessonsController@clone',
    ];

}
