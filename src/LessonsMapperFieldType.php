<?php namespace Defr\LessonsMapperFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldType;
use Defr\LessonsModule\Course\Contract\CourseRepositoryInterface;

/**
 * Class LessonsMapperFieldType command
 *
 * @category Streams_Platform_Addon
 * @package  LayoutFieldType
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class LessonsMapperFieldType extends FieldType
{

    /**
     * The database column type.
     *
     * @var  string
     */
    protected $columnType = 'text';

    /**
     * The input view.
     *
     * @var  string
     */
    protected $inputView = 'defr.field_type.lessons_mapper::input';

    /**
     * The field rules
     *
     * @var  array
     */
    protected $rules = [
        'array',
    ];

    /**
     * Gets the course.
     *
     * @return  JSONString  The course.
     */
    public function getCourse()
    {
        return app(CourseRepositoryInterface::class)
            ->find(app('request')->get('course'))
            ->toJson();
    }

}
