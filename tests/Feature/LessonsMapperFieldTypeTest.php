<?php namespace Defr\LessonsMapperFieldType\Test\Feature;

/**
 * Class LessonsMapperFieldTypeTest command
 *
 * @category Streams_Platform_Addon
 * @package  LayoutFieldType
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class LessonsMapperFieldTypeTest extends \TestCase
{

    public function testFeature()
    {
        $this->markTestSkipped('Not implemented.');
    }
}
